/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto2;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import proyecto2.Parser1;

/**
 *
 * @author kenia
 */
public class Ventana1 extends javax.swing.JFrame {

   public static ArrayList<Columna> ListaColumnas; 
   public static ArrayList<String> ListaCeldas;
   public static ArrayList<Errores> ListaErrores;
   public static ArrayList<Fila> ListaFilas;
   public static String path, nombre, fecha; 
   JButton botonAbrir;/*declaramos el objeto Boton*/
   JButton botonGuardar;
   JFileChooser fileChooser; /*Declaramos el objeto fileChooser*/
   String texto;
   String archivo;    
   public static JPanel panel1;
   int numFilas=51; 
   int numColum=31;
   int letra=0;
   int letra1=0;
   int letra2 =0;
   int pos;
   public Celdas Hoja[][] = new Celdas[numFilas][numColum];
   public String FrontEnd[][] = new String [numFilas][numColum];  
   public Funcion BackEnd[][] = new Funcion[numFilas][numColum]; 
   public static ArrayList<String> ListaErrores1;
  // this.tokensList = new ArrayList();
   public static ArrayList<String> ListaErrores2;
   public static ArrayList<String> ListaErrores3;
   public static ArrayList<String> ListaErrores4;
   public static ArrayList<String> ListaErrores5;
   
   
    public Ventana1() {
        initComponents();
       // ListaErrores1 = null;
        fileChooser=new JFileChooser();
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        panel1 = new JPanel();
        JScrollPane jsp = new JScrollPane();
        jsp.setBounds(10, 10, 1250, 700);
        jsp.setViewportView(panel1);
        this.add(jsp);
        
       // this.add(panel1);
        iniciar();
        
        panel1.setPreferredSize(new Dimension(3000,2000));

        
   
    }
    
   
    private void iniciar(){
     panel1.removeAll();
     panel1.setLayout(new GridLayout(numFilas,numColum));
     
      for (int i=0;i<numFilas;i++){
        for (int j=0;j<numColum;j++){ 
             Hoja[i][j]=new Celdas();
             Hoja[i][j].setBackground(Color.WHITE);
            Hoja[i][j].setFocusCycleRoot(rootPaneCheckingEnabled);
          
             Hoja[i][j].setSize(60, 25);
             Hoja[i][j].setBorder(BorderFactory.createLineBorder(Color.pink));
             Hoja[i][j].setVisible(true);
             panel1.add( Hoja[i][j]); 
           
//             Hoja[i][j].setText(Integer.toString(Serp[i][j]));
        }    
       }
     
      Font font = new Font("Arial", Font.BOLD, 20);
      pos=1;
      for ( int i=0;i<numFilas;i++){ 
         if(pos<51){
         Hoja[pos][0].setFont(font);    
         Hoja[pos][0].setText(String.valueOf(i+1));
         }

         pos++;
        }    
       
      
      letra=65;
      letra1=65;
      letra2 = 65; 
      pos = 1;
      
       for ( int j=0;j<numColum;j++){   
               
        if(pos<31){
         if(letra>90)
         {
         Hoja[0][pos].setFont(font);
         Hoja[0][pos].setText( String.valueOf(Character.toChars(letra1))+String.valueOf(Character.toChars(letra2)));
         letra2++;
         }
         else
         {
         Hoja[0][pos].setFont(font);
         Hoja[0][pos].setText(String.valueOf(Character.toChars(letra)));
         letra++;
         }
        }

        pos++;             
       }   
       
       
      }
     
     
    private String AbrirArchivo() {

            String aux=""; 		
            texto="";

            try
            {
                
                /*llamamos el metodo que permite cargar la ventana*/
            FileNameExtensionFilter filter = new FileNameExtensionFilter("Excel", "olc1arch","olc1backend","olc1frontend","olc1func", "olc1funesp");
            fileChooser.setFileFilter(filter);
            fileChooser.showOpenDialog(this);
            /*abrimos el archivo seleccionado*/
            File abre=fileChooser.getSelectedFile();

                /*recorremos el archivo, lo leemos para plasmarlo
                 *en el area de texto*/
                if(abre!=null)
                { 			
                    archivo=abre.getPath();
                    FileReader archivos=new FileReader(abre);
                    BufferedReader lee=new BufferedReader(archivos);
                    while((aux=lee.readLine())!=null)
                        {
                         texto+= aux+ "\n";
                        }

                    lee.close();
                } 			
            }
            catch(IOException ex)
            {
              JOptionPane.showMessageDialog(null,ex+"" +
                            "\nNo se ha encontrado el archivo",
                            "ADVERTENCIA!!!",JOptionPane.WARNING_MESSAGE);
            }
                    return texto;
    }

    /**
     * Guardamos el archivo del area 
     * de texto en el equipo
     */
    private void GuardarArchivo() {

            try
            {
                String nombre;
                JFileChooser file=new JFileChooser();
                file.showSaveDialog(this);
                File guarda =file.getSelectedFile();

                if(guarda !=null)
                {
                nombre = file.getSelectedFile().getName();
                /*guardamos el archivo y le damos el formato directamente,
                 * si queremos que se guarde en formato doc lo definimos como .doc*/
                FileWriter  save=new FileWriter(guarda);
              //  save.write(AreaDeTexto.getText());
                save.close();
                JOptionPane.showMessageDialog(null,
                "El archivo se a guardado Exitosamente",
                "Información",JOptionPane.INFORMATION_MESSAGE);
               }
             }
       catch(IOException ex)
       {
             JOptionPane.showMessageDialog(null,
             "Su archivo no se ha guardado",
             "Advertencia",JOptionPane.WARNING_MESSAGE);
       }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem_Nuevo = new javax.swing.JMenuItem();
        jMenuItem_Abrir = new javax.swing.JMenuItem();
        jMenuItem_Frontend = new javax.swing.JMenuItem();
        jMenuItem_Backend = new javax.swing.JMenuItem();
        jMenuItem_funciones = new javax.swing.JMenuItem();
        jMenuItem_Guardar = new javax.swing.JMenuItem();
        jMenuItem_GuardarComo = new javax.swing.JMenuItem();
        jMenuItem_Salir = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenuItem10 = new javax.swing.JMenuItem();
        jMenuItem_MostrarBE = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jMenuBar1.setFont(new java.awt.Font("Ubuntu", 3, 18)); // NOI18N

        jMenu1.setText("Archivo  ");
        jMenu1.setFont(new java.awt.Font("Ubuntu", 3, 18)); // NOI18N

        jMenuItem_Nuevo.setText("Nuevo");
        jMenuItem_Nuevo.setToolTipText("");
        jMenuItem_Nuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_NuevoActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem_Nuevo);

        jMenuItem_Abrir.setText("Abrir");
        jMenuItem_Abrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_AbrirActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem_Abrir);

        jMenuItem_Frontend.setText("Frontend");
        jMenuItem_Frontend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_FrontendActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem_Frontend);

        jMenuItem_Backend.setText("Backend");
        jMenuItem_Backend.setToolTipText("");
        jMenuItem_Backend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_BackendActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem_Backend);

        jMenuItem_funciones.setText("Funciones");
        jMenuItem_funciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_funcionesActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem_funciones);

        jMenuItem_Guardar.setText("Guardar");
        jMenu1.add(jMenuItem_Guardar);

        jMenuItem_GuardarComo.setText("Guardar Como");
        jMenu1.add(jMenuItem_GuardarComo);

        jMenuItem_Salir.setText("Salir");
        jMenu1.add(jMenuItem_Salir);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Ejecutar   ");
        jMenu2.setFont(new java.awt.Font("Ubuntu", 3, 18)); // NOI18N

        jMenuItem7.setText("Add Funciones");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem7);

        jMenuItem8.setText("Add Func Especiales");
        jMenu2.add(jMenuItem8);

        jMenuItem9.setText("Errores");
        jMenuItem9.setToolTipText("");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem9);

        jMenuItem10.setText("Tabla De Símbolos");
        jMenuItem10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem10ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem10);

        jMenuItem_MostrarBE.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F2, 0));
        jMenuItem_MostrarBE.setText("Mostrar BE");
        jMenuItem_MostrarBE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_MostrarBEActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem_MostrarBE);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Ayuda");
        jMenu3.setFont(new java.awt.Font("Ubuntu", 3, 18)); // NOI18N
        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1250, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 675, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem_AbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_AbrirActionPerformed
        // TODO add your handling code here: String arch=AbrirArchivo();
      String arch=AbrirArchivo();
       System.out.println("Compilando");
        try {
                // TODO add your handling code here:
         new Parser1(new Scanner1(new FileReader(archivo))).parse();
         System.out.println("si entra al try");
         Frontend();
         Backend();
         Funciones();
                
         } catch (Exception ex) {
           System.out.println("Terminó de Compilar");
                
         }
        
       //  System.out.println(path +" "+ nombre +" "+ fecha);
      
    }//GEN-LAST:event_jMenuItem_AbrirActionPerformed

    private void jMenuItem_FrontendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_FrontendActionPerformed
        // TODO add your handling code here:
        
       String arch=AbrirArchivo();
       System.out.println("Compilando");
        try {
                // TODO add your handling code here:
         new Parser2(new Scanner2(new FileReader(archivo))).parse();
         System.out.println("si entra al try");
         MostrarFrontend(); 
                
         } catch (Exception ex) {
           System.out.println("Terminó de Compilar");              
         }
        
   
      
    }//GEN-LAST:event_jMenuItem_FrontendActionPerformed

    public void Frontend(){
       archivo = path+"/Ejemplo.olc1frontend";
       System.out.println("Compilando");
        try {
                // TODO add your handling code here:
         new Parser2(new Scanner2(new FileReader(archivo))).parse();
         System.out.println("si entra al try");
         MostrarFrontend(); 
                
         } catch (Exception ex) {
           System.out.println("Terminó de Compilar");              
         }
    
    }
    
    public void MostrarFrontend(){
      
        for (int k = 0; k<ListaColumnas.size(); k++) {
              System.out.println(ListaColumnas.get(k).nombre);
              String name = ListaColumnas.get(k).nombre;
              int tamano = ListaColumnas.get(k).celdas.size();
             
              
          for (int j=0;j<numColum;j++){ 
              String letra =  Hoja[0][j].getText();
              String celda;
              if(letra.equals(name)){
               for (int m = 0; m < tamano; m++) {
                 celda = ListaColumnas.get(k).celdas.get(m);  
                 Hoja[m+1][j].setText(celda);
                 FrontEnd[m+1][j] = celda;
                 System.out.println(celda);
               } 
               j=numColum;      
              }                   
          }        
        }
        
        
            
    }
    
    public void Backend(){
       archivo = path+"/Ejemplo.olc1backend";
       System.out.println("Compilando");
        try {
                // TODO add your handling code here:
         new Parser3(new Scanner3(new FileReader(archivo))).parse();
         System.out.println("si entra al try");
       
                
         } catch (Exception ex) {
           System.out.println("Terminó de Compilar");              
         }
    
    }
    
    public void MostrarBackend() {
      
         for (int k = 0; k<ListaFilas.size(); k++) {
              System.out.println(ListaFilas.get(k).nombre);
              String name = ListaFilas.get(k).nombre;
              int tamano = ListaFilas.get(k).celdas.size();
                         
          for (int j=0;j<numFilas;j++){ 
              String letra =  Hoja[j][0].getText();
              String operacion, cadena,p1,p2;
              Integer P1,P2;
              if(letra.equals(name)){
               for (int m = 0; m < tamano; m++) {
                p1 = ListaFilas.get(k).celdas.get(m).parametro1;  
                p2 = ListaFilas.get(k).celdas.get(m).parametro2; 
                
                
                 operacion = ListaFilas.get(k).celdas.get(m).tipo; 
                 if(operacion!=""){
                 cadena = operacion+ "="+"("+p1+","+p2+")";
                 Hoja[j][m+1].setText(cadena);
                 FrontEnd[m+1][j] = cadena;
                 System.out.println(cadena);
                 }
                
               
           /*     if(p1.contains("[a-zA-Z]+")){
                 p1 =  String.valueOf(p1.charAt(0));
                 
                 for (int n=0;n<numColum;n++){ 
                  String letra1 =  Hoja[0][n].getText();
                  String celda;
                  if(letra1.equals(p1)){
                     P1= Integer.valueOf(Hoja[j][n].getText());
                     P2 = Integer.valueOf(p2);
                     Hoja[j][m+1].setText(Ops(operacion, P1, P2).toString());
                      
                  }
                }
                 
                }*/
                
               } 
               j=numFilas;      
              }                   
          }        
        }
            
            
       
    }
    
    public Integer Ops(String Op, Integer p1, Integer p2){
    
        Integer resultado = 0;
        if(Op.equals("Suma")){
        resultado = p1+p2;
        }
        
         if(Op.equals("Multiplicacion")){
        resultado = p1*p2;
        }
         
          if(Op.equals("Resta")){
        resultado= p1-p2;
        }
          
            if(Op.equals("Division")){
        resultado= p1/p2;
        }
          
       return resultado;   
    }
    
    private void jMenuItem_BackendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_BackendActionPerformed
        // TODO add your handling code here:
      Backend();
       
        
    }//GEN-LAST:event_jMenuItem_BackendActionPerformed

    private void jMenuItem_MostrarBEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_MostrarBEActionPerformed
        // TODO add your handling code here:
        MostrarBackend();
    }//GEN-LAST:event_jMenuItem_MostrarBEActionPerformed

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed
        // TODO add your handling code here:
        Errores();
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    private void jMenuItem_funcionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_funcionesActionPerformed
        // TODO add your handling code here:
        Funciones();
    }//GEN-LAST:event_jMenuItem_funcionesActionPerformed

    private void jMenuItem10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem10ActionPerformed
        // TODO add your handling code here:
        
        Func();
    }//GEN-LAST:event_jMenuItem10ActionPerformed

    private void jMenuItem_NuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_NuevoActionPerformed
        // TODO add your handling code here:
        iniciar();
    }//GEN-LAST:event_jMenuItem_NuevoActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    public void Funciones(){
    archivo = path+"/Ejemplo.olc1func";
       System.out.println("Compilando");
        try {
                // TODO add your handling code here:
         new Parser4(new Scanner4(new FileReader(archivo))).parse();
         System.out.println("si entra al try");
       
                
         } catch (Exception ex) {
           System.out.println("Terminó de Compilar");              
         }
     }
    
    
    public void Errores()
 {
     try{         
         FileWriter archivo = new FileWriter("Errores.html"); 

          PrintWriter escribir = new PrintWriter(archivo);
         
         escribir.println("<html>");
         escribir.println("<head>");
         escribir.println("<title>Errores</title>");
         escribir.println("</head>");
         escribir.println("<body bgcolor='FFFFFF'>");
         escribir.println("<font color='FFFFFF' face='CenturyGothic'><h1><strong><center>Errores</center></strong></h1><br/>");
         escribir.println("<table align= center width=50% height=10% bordercolor=''FFFFFF\" border=1 cellspacing='5' cellpadding='5'>");
         escribir.println("<tr>");
         escribir.println("<td align=center bgcolor='FFFFFF'><font color='2E2E2E'><strong>Error</strong></font></td>");
         escribir.println("<td align=center bgcolor='FFFFFF'><font color='2E2E2E'><strong>Tipo</font></strong></td>");
         escribir.println("</tr>");
        
         
         
        if(!Scanner1.tokensListErrores.isEmpty()){
        }
        else{
         for(int k = 0; k<Scanner1.tokensListErrores.size(); k++)
         {
         escribir.println("<tr>");
         escribir.println("<td align=center bgcolor='FFFFFF'><font color='2E2E2E'><strong>"+Scanner1.tokensListErrores.get(k).toString()+"</font></strong></td>");
         escribir.println("<td align=center bgcolor='FFFFFF'><font color='2E2E2E'><strong>"+"Lexico, Archivo 1"+"</font></strong></td>");

         escribir.println("</tr>");
              
         }
        }

         System.out.println(path);
          
        if(!ListaErrores.isEmpty()){
         for(int k = 0; k<ListaErrores.size(); k++)
         {
         escribir.println("<tr>");
         escribir.println("<td align=center bgcolor='FFFFFF'><font color='2E2E2E'><strong>"+ListaErrores.get(k).error+"</font></strong></td>");
         escribir.println("<td align=center bgcolor='FFFFFF'><font color='2E2E2E'><strong>"+"Sintac, Archivo 1"+"</font></strong></td>");

         escribir.println("</tr>");
              
         }
        }
        
        
        if(Scanner2.tokensListErrores.isEmpty()){
        } else {
            for(int k = 0; k<Scanner2.tokensListErrores.size(); k++)
            {
                escribir.println("<tr>");
                escribir.println("<td align=center bgcolor='FFFFFF'><font color='2E2E2E'><strong>"+Scanner2.tokensListErrores.get(k).toString()+"</font></strong></td>");
                escribir.println("<td align=center bgcolor='FFFFFF'><font color='2E2E2E'><strong>"+"Lexico, Archivo 1"+"</font></strong></td>");
                
                escribir.println("</tr>");
                
            }
         }
        
      
        
        if(!Scanner3.tokensListErrores.isEmpty()){
        }
        else{
         for(int k = 0; k<Scanner3.tokensListErrores.size(); k++)
         {
         escribir.println("<tr>");
         escribir.println("<td align=center bgcolor='FFFFFF'><font color='2E2E2E'><strong>"+Scanner3.tokensListErrores.get(k).toString()+"</font></strong></td>");
         escribir.println("<td align=center bgcolor='FFFFFF'><font color='2E2E2E'><strong>"+"Lexico, Archivo 1"+"</font></strong></td>");

         escribir.println("</tr>");
              
         }
        }
        
        
        
        
    /*  
      }*/
         
         
         
     
        
        
         escribir.println("</table>");
         escribir.println("</body>");
         escribir.println("</html>");
         escribir.close();
        
         
       
    
       //  Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler "+"Errores.html");
     }catch(Exception e){
         e.printStackTrace();
     }
     
     
     
       try {
     File path = new File ("/home/kenia/Documentos/Compi1/[Compi1]Proyecto2_201212623/Errores.html");
     Desktop.getDesktop().open(path);
    }catch (IOException ex) {
     ex.printStackTrace();
    }
         

 }
    
    
 public void Func()
 {
     try{         
         FileWriter archivo = new FileWriter("Funciones.html"); 

          PrintWriter escribir = new PrintWriter(archivo);
         
         escribir.println("<html>");
         escribir.println("<head>");
         escribir.println("<title>FUnciones</title>");
         escribir.println("</head>");
         escribir.println("<body bgcolor='FFFFFF'>");
         escribir.println("<font color='FFFFFF' face='CenturyGothic'><h1><strong><center>Funciones</center></strong></h1><br/>");
         escribir.println("<table align= center width=50% height=10% bordercolor=''FFFFFF\" border=1 cellspacing='5' cellpadding='5'>");
         escribir.println("<tr>");
         escribir.println("<td align=center bgcolor='FFFFFF'><font color='2E2E2E'><strong>Funcion</strong></font></td>");
         escribir.println("</tr>");
        
         
         
        for (int k = 0; k<ListaFilas.size(); k++) {
              System.out.println(ListaFilas.get(k).nombre);
              String name = ListaFilas.get(k).nombre;
              int tamano = ListaFilas.get(k).celdas.size();
            
              String operacion, cadena,p1,p2;
              Integer P1,P2;
            
               for (int m = 0; m < tamano; m++) {
                p1 = ListaFilas.get(k).celdas.get(m).parametro1;  
                p2 = ListaFilas.get(k).celdas.get(m).parametro2; 
                
                
                 operacion = ListaFilas.get(k).celdas.get(m).tipo; 
                 if(operacion!=""){
                 cadena = operacion+ "="+"("+p1+","+p2+")";
                  escribir.println("<tr>");
         escribir.println("<td align=center bgcolor='FFFFFF'><font color='2E2E2E'><strong>"+cadena+"</font></strong></td>");

         escribir.println("</tr>");
                 }
                 
               }
        }
         
        
        
     
         escribir.println("</table>");
         escribir.println("</body>");
         escribir.println("</html>");
         escribir.close();
       
         
       
    
       //  Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler "+"Errores.html");
     }catch(Exception e){
         e.printStackTrace();
     }
     
     
     
       try {
     File path = new File ("/home/kenia/Documentos/Compi1/[Compi1]Proyecto2_201212623/Funciones.html");
     Desktop.getDesktop().open(path);
    }catch (IOException ex) {
     ex.printStackTrace();
    }
         

 }   
    
   
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Ventana1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Ventana1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Ventana1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Ventana1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Ventana1().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JMenuItem jMenuItem_Abrir;
    private javax.swing.JMenuItem jMenuItem_Backend;
    private javax.swing.JMenuItem jMenuItem_Frontend;
    private javax.swing.JMenuItem jMenuItem_Guardar;
    private javax.swing.JMenuItem jMenuItem_GuardarComo;
    private javax.swing.JMenuItem jMenuItem_MostrarBE;
    private javax.swing.JMenuItem jMenuItem_Nuevo;
    private javax.swing.JMenuItem jMenuItem_Salir;
    private javax.swing.JMenuItem jMenuItem_funciones;
    // End of variables declaration//GEN-END:variables
}
