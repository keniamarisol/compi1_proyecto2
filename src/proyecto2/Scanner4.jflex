package proyecto2;
import java_cup.runtime.Symbol;
import java.util.ArrayList;


%%

%cupsym tabla_simbolos4
%class Scanner4
%cup
%public
%unicode
%line
%column
%char
%ignorecase

%init{ /* The %init directive allows us to introduce code in the class constructor. We are using it for initializing our variables */
this.tokensList = new ArrayList();
this.tokensListErrores = new ArrayList();

%init}

%{
public static ArrayList tokensList;
public static ArrayList tokensListErrores = null;

%}

nombre= [a-zA-Z]+ [0-9]*

valor= [0-9]+

idvalor= ([a-zA-Z]|[0-9])+

logico= ("<"|">")?

%%

/*PALABRAS RESERVADAS*/

"new" {return new Symbol(tabla_simbolos4.tnuevo, yychar,yyline); }
"entero" {return new Symbol(tabla_simbolos4.tentero, yychar,yyline); }
"if" {return new Symbol(tabla_simbolos4.tcicloif, yychar,yyline); }
"while" {return new Symbol(tabla_simbolos4.tciclowhile, yychar,yyline); }
"for"  {return new Symbol(tabla_simbolos4.tciclofor, yychar,yyline); }
"return" {return new Symbol(tabla_simbolos4.tretornar, yychar,yyline); }
"Funciones" {return new Symbol(tabla_simbolos4.tfunciones, yychar,yyline); }

/* SEPARADOR */
"{"  {return new Symbol(tabla_simbolos4.tllavea, yychar,yyline); }
"}"  {return new Symbol(tabla_simbolos4.tllavec, yychar,yyline); }
"["  {return new Symbol(tabla_simbolos4.tcorchetea, yychar,yyline); }
"]"  {return new Symbol(tabla_simbolos4.tcorchetec, yychar,yyline); }
","  {return new Symbol(tabla_simbolos4.tcoma, yychar,yyline); }
"("  {return new Symbol(tabla_simbolos4.tparentesisa, yychar,yyline); }
")"  {return new Symbol(tabla_simbolos4.tparentesisc, yychar,yyline); }
";"  {return new Symbol(tabla_simbolos4.tpuntocoma, yychar,yyline); }
"==" {return new Symbol(tabla_simbolos4.tasignacion, yychar,yyline); }
"="  {return new Symbol(tabla_simbolos4.tigual, yychar,yyline); }
"+"  {return new Symbol(tabla_simbolos4.tsuma, yychar,yyline); }
"-"  {return new Symbol(tabla_simbolos4.tresta, yychar,yyline); }
"*"  {return new Symbol(tabla_simbolos4.tmultiplicacion, yychar,yyline); }
"/"  {return new Symbol(tabla_simbolos4.tdivision, yychar,yyline); }
":"  {return new Symbol(tabla_simbolos4.tdospuntos, yychar,yyline); }
"$"  {return new Symbol(tabla_simbolos4.tcomentario, yychar,yyline); }

{nombre} {return new Symbol(tabla_simbolos4.ernombre, yychar,yyline, new String(yytext())); }
{valor} {return new Symbol(tabla_simbolos4.ervalor, yychar,yyline, new String(yytext())); }
{logico} {return new Symbol(tabla_simbolos4.erlogico, yychar,yyline, new String(yytext())); }
{idvalor} {return new Symbol(tabla_simbolos4.eridvalor, yychar,yyline, new String(yytext())); }
    
/* BLANCOS */
[ \t\r\f\n]+ { /* Se ignoran */}



/* CUAQUIER OTRO */
. {this.tokensListErrores.add("Token: "+yytext());
                      System.out.println("Error léxico: "+yytext());}