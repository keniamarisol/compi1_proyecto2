package proyecto2;
import java_cup.runtime.Symbol;
import java.util.ArrayList;


%%

%cupsym tabla_simbolos2
%class Scanner2
%cup
%public
%unicode
%line
%column
%char
%ignorecase

%init{ /* The %init directive allows us to introduce code in the class constructor. We are using it for initializing our variables */
this.tokensList = new ArrayList();
this.tokensListErrores = new ArrayList();
%init}

%{

public static ArrayList tokensList;
public static ArrayList tokensListErrores = null;
%}


numeros=[0-9]+|[\-][0-9]+
contenido = [a-zA-ZáéíóúÁÉÍÓÚÑñ0-9\.\_]+  
nombre = [a-zA-Z] | [aA][a-dA-D]?
/*[a-zA-ZáéíóúÁÉÍÓÚÑñ0-9\.\_]+ | [0-9]+*/
/*comillas = [\"]*/
%%



/*PALABRAS RESERVADAS*/


"Columna" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tcolumna, yycolumn,yyline, new String(yytext()));}

"\[" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tcorchetea, yycolumn,yyline);}

"\]" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tcorchetec, yycolumn,yyline);}

"\{" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tllavea, yycolumn,yyline);}

"\}" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tllavec, yycolumn,yyline);}

":" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tdospuntos, yycolumn,yyline);}

";" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tpuntoycoma, yycolumn,yyline);}

"," { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.tcoma, yycolumn,yyline);}


/*NUMEROS*/

/*LETRAS*/
{nombre} { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.ername, yycolumn,yyline, new String(yytext()));}

{numeros} { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.ernumbers, yycolumn,yyline, new String(yytext()));}


{contenido} { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos2.ercontenido, yycolumn,yyline, new String(yytext()));}





/* CUALQUIER OTRO */
[ \t\r\f\n]+             { /* se ignoran  */}
.                       {System.out.println("Error léxico: "+yytext());
 System.out.println("Error léxico: "+yytext());}