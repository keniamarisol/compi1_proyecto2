package proyecto2;
import java_cup.runtime.Symbol;
import java.util.ArrayList;


%%

%cupsym tabla_simbolos1
%class Scanner1
%cup
%public
%unicode
%line
%column
%char
%ignorecase

%init{ /* The %init directive allows us to introduce code in the class constructor. We are using it for initializing our variables */
this.tokensList = new ArrayList();
this.tokensListErrores = new ArrayList();
%init}

%{

public static ArrayList tokensList;
public static ArrayList tokensListErrores;;
%}

nombre= [a-zA-ZáéíóúÁÉÍÓÚÑñ0-9\[\]\.\_]+
numeros=[0-9\-\/]+|[\-][0-9]+
comillas = [\"]
path= ([\/]{nombre})+
%%



/*PALABRAS RESERVADAS*/


"Proyecto" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tproyecto, yycolumn,yyline, new String(yytext()));}

"Path" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tpath, yycolumn,yyline, new String(yytext()));}
 
"Nombre" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tnombre, yycolumn,yyline, new String(yytext()));}

"Fecha" {this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tfecha, yycolumn,yyline, new String(yytext()));}

"[" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tcorchetea, yycolumn,yyline);}

"]" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tcorchetec, yycolumn,yyline);}

":" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tdospuntos, yycolumn,yyline);}

";" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tpuntoycoma, yycolumn,yyline);}


{comillas} { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.tcomillas, yycolumn,yyline, new String(yytext()));}

/*NUMEROS*/
{numeros} { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.ernumbers, yycolumn,yyline, new String(yytext()));}

/*LETRAS*/
{nombre} { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.ername, yycolumn,yyline,new String(yytext()));}


{path} { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos1.erpath, yycolumn,yyline,new String(yytext()));}


/* CUALQUIER OTRO */
[ \t\r\f\n]+             { /* se ignoran  */}
.                       {this.tokensListErrores.add("Token: "+yytext());
                      System.out.println("Error léxico: "+yytext());}