package proyecto2;
import java_cup.runtime.Symbol;
import java.util.ArrayList;


%%

%cupsym tabla_simbolos3
%class Scanner3
%cup
%public
%unicode
%line
%column
%char
%ignorecase

%init{ /* The %init directive allows us to introduce code in the class constructor. We are using it for initializing our variables */
this.tokensList = new ArrayList();
this.tokensListErrores = new ArrayList();
%init}

%{
public static ArrayList tokensList;
public static ArrayList tokensListErrores = null;
%}

parametro = [A-Z][0-9]+ 
nombre = [a-zA-Z]+
numeros = [0-9]+
coma = [\,]?

%%

/*PALABRAS RESERVADAS*/


"\(" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos3.tparentesisa, yycolumn,yyline, new String(yytext()));}

"\)" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos3.tparentesisc, yycolumn,yyline, new String(yytext()));}

"\{" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos3.tllavea, yycolumn,yyline, new String(yytext()));}

"\}" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos3.tllavec, yycolumn,yyline, new String(yytext()));}

":" { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos3.tdospuntos, yycolumn,yyline, new String(yytext()));}

"," { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos3.tcoma, yycolumn,yyline, new String(yytext()));}

{coma} { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos3.tcomaop, yycolumn,yyline, new String(yytext()));}


/*LETRAS*/

{numeros} { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos3.ernumbers, yycolumn,yyline, new String(yytext()));}

{parametro} { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos3.erparametro, yycolumn,yyline, new String(yytext()));}

{nombre} { this.tokensList.add("Token: "+yytext());
return new Symbol(tabla_simbolos3.ernombre, yycolumn,yyline, new String(yytext()));}


/* CUALQUIER OTRO */
[ \t\r\f\n]+             { /* se ignoran  */}
.                       {System.out.println("Error léxico: "+yytext());
 System.out.println("Error léxico: "+yytext());}